import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

export const baseURL = 'http://localhost:5000';

export const axiosInstance = axios.create({
    baseURL: baseURL,
    timeout: 30000,
});