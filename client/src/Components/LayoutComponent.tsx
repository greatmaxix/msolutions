import * as React from "react";
import FooterComponent from "./Footer/FooterComponent";
import NavbarComponent from "./Navbar/NavbarComponent";
import { ContextValueType, LayoutContext } from "./LayoutContext";
import axios from "axios";
import { axiosInstance } from "./axiosInstance";

export const DefaultLanguage: SiteLang = { name: 'English', code: 'en-EN' };

export type SiteLang = {
    name: string,
    code: string
}

export const ListOfAvailableLang: SiteLang[] = [
    { name: 'English', code: 'en-EN' },
    { name: 'Русский', code: 'ru-RU' },
]

type LayoutComponentState = {
    languageValue: SiteLang | null,
    value: ContextValueType | undefined
}

export function findFromList(value: string) : SiteLang {
    let foundLanguageOrDefault : SiteLang | undefined = ListOfAvailableLang.find((element: SiteLang) => element.code === value); //try to find user choosen language from list
    if (!foundLanguageOrDefault) foundLanguageOrDefault = DefaultLanguage; //set to default if not found
    return foundLanguageOrDefault;
}

export default class LayoutComponent extends React.Component<any, LayoutComponentState> {
    constructor(props: any) {
        super(props);
        this.state = {
            languageValue: DefaultLanguage,
            value: {
                langCode: DefaultLanguage.code
            }
        }
    }

    componentDidMount() {
        this.reloadAsync();
    }

    private async reloadAsync() {
    }

    private async updateLanguageValue(stringValue: string) {
        let siteLang: SiteLang = findFromList(stringValue);
    
        let newContextValue = this.state.value;
        if (newContextValue) {
            newContextValue.langCode = siteLang.code;
        }
        else {
            newContextValue = {
                langCode : siteLang.code
            }
        }

        axios.interceptors.request.use(function (config) {
            config.params['langCode'] = siteLang.code;
            return config;
        }, function (error) {
            return Promise.reject(error);
        });

        this.setState({
            languageValue: siteLang,
            value: newContextValue
        });
    }

    render() {
        return (
            <LayoutContext.Provider value={this.state.value}>
                <NavbarComponent onLanguageUpdate={this.updateLanguageValue.bind(this)}/>
                    <section>
                        <div className="container-fluid px-0">
                            {this.props.children}
                        </div>
                    </section>
                <FooterComponent/>
            </LayoutContext.Provider>
        )
    }
}