import * as React from "react";
import { Link } from "react-router-dom";
import { DefaultLanguage, findFromList, ListOfAvailableLang, SiteLang } from "../LayoutComponent";
import LayoutContext, { LayoutTabElem } from "../LayoutContext";

export type NavbarState = {
    pathname: any,
    siteLanguageCode: string
}

export type NavbarProps = {
    onLanguageUpdate: (value: string) => void,
}

export default class NavbarComponent extends React.Component<NavbarProps, NavbarState> {
    constructor(props: any) {
        super(props);
        this.state = {
            pathname: "",
            siteLanguageCode: "",
        }
    }

    componentDidMount() {
        this.reloadAsync();
    }

    private reloadAsync() {
        const browserLang = navigator.language;
        let foundLanguageOrDefault = findFromList(browserLang);

        if (foundLanguageOrDefault) {
            this.setState({ siteLanguageCode: foundLanguageOrDefault.code });
        }
    }

    private handleLanguageSwitch(event: any) {
        let languageCodeFromId = event.target.id;
        if (languageCodeFromId) {
            this.setState({ siteLanguageCode: languageCodeFromId });
            this.props.onLanguageUpdate(languageCodeFromId);
        }
    }

    private renderLanguageSelectorElement(elem: SiteLang, index: number) {
        let elemClassList = this.state.siteLanguageCode === elem.code ? "nav-item px-2 border-bottom border-success" : "nav-item px-2 border-bottom";
        return (
            <li className={elemClassList} key={index}>
                <h5><label id={elem.code} onClick={this.handleLanguageSwitch.bind(this)}>{elem.name}</label></h5>
            </li>
        );
    }

    render() {
        return (
            <header>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <LayoutContext.Consumer>
                        {
                            value => (
                                value ?
                                    <>
                                        <Link className="navbar-brand" to='/'>{"Change this"}</Link>
                                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                            <span className="navbar-toggler-icon"></span>
                                        </button>
                                        <div className="collapse navbar-collapse" id="navbarNav">
                                            <ul className="navbar-nav mr-auto">
                                                {/* {value.textCollection.commonText.tabs.map((tabElem: LayoutTabElem, index) => {
                                                    return (
                                                        <li className="nav-item" key={index}>
                                                            <Link className="nav-link" to={tabElem.url}>{tabElem.tabName}</Link>
                                                        </li>);
                                                })} */}
                                            </ul>
                                            <ul className="navbar-nav">
                                            {
                                                ListOfAvailableLang.map((currLang: SiteLang, index) => {
                                                    return this.renderLanguageSelectorElement(currLang, index);
                                                })
                                            }
                                            </ul>
                                        </div>
                                        
                                    </>
                                    : ""
                            )
                        }
                    </LayoutContext.Consumer>
                </nav>
            </header>
        )
    }
}

NavbarComponent.contextType = LayoutContext;