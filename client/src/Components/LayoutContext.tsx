import React from 'react';
export type ContextValueType = {
    langCode: string
}

export type LayoutTabElem = {
    url: string,
    tabName: string
}

export const LayoutContext = React.createContext<ContextValueType | undefined>(undefined);
export default LayoutContext;