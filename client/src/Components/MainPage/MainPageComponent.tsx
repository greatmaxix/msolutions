import axios from "axios";
import * as React from "react";
import { axiosInstance } from "../axiosInstance";
import LayoutContext from "../LayoutContext";

export type MainPageComponenteProps = {
}

export type MainPageComponentState = {
}

export default class MainPageComponent extends React.Component<MainPageComponenteProps, MainPageComponentState> {
    constructor(props: any) {
        super(props);
        this.state = {
        }
    }

    private handleClick() {
        let axios2 = axios.create({
            baseURL: "http://localhost:5000/test",
        })
        axios2.interceptors.request.use(function (config) {
            config.headers['langCode'] = 'annasdnsad';
            return config;
        }, function (error) {
            return Promise.reject(error);
        });
        axios2.get("");
    }

    render() {
        return (
            <button onClick={this.handleClick.bind(this)}>Hello motherfucker!</button>
        );
    }
}

MainPageComponent.contextType = LayoutContext;