import React from 'react';
import LayoutComponent from './Components/LayoutComponent';
import MainPageComponent from './Components/MainPage/MainPageComponent';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const App: React.FC = () => {
    return (
        <Router>
            <LayoutComponent>
                <Switch>
                    <Route exact path="/" component={MainPageComponent} />
                </Switch>
            </LayoutComponent>
        </Router>
    );
}

export default App;