# MSolutions

Client side: create-react-app

# To start back-end

You can run docker-compose up in /server to start docker mysql container or use your own setup and edit connection string in env file

npm install - to install dependencies

npm start - to start server

# To start front-end:

npm install

npm run dev