import dotenv from "dotenv";
import express, { Request, Response, NextFunction, Router } from "express";
import cors from "cors";

dotenv.config({
    path: ".env",
});

const app = express();
const port = process.env.APP_PORT || 5000;

app.use(cors());

app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.get("/test", (req: Request, res: Response) => {
    // console.log(req.body);
    try {
        res.send("works!");
    }
    catch(error) {
        console.log(error);
    }
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});